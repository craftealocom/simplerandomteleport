package com.craftealo.RandomTeleport;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Logger;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import com.craftealo.RandomTeleport.commands.RandomTeleport;

public class Main extends JavaPlugin {
	public static Main instance;

	public void onEnable() {
		instance = this;
		registerCommands();
		registerConfig();
		Logger logger = Logger.getLogger("Minecraft");
		logger.info("[SimpleRandomTeleporter] " + lang.getString("CONSOLE_MESSAGE_ENABLED").split("%s")[0]
				+ getConfig().getString("version") + lang.getString("CONSOLE_MESSAGE_ENABLED").split("%s")[1]);
	}

	public void onDisable() {
		Logger logger = Logger.getLogger("Minecraft");
		logger.info("[SimpleRandomTeleporter]" + lang.getString("CONSOLE_MESSAGE_DISABLED").split("%s")[0]
				+ getConfig().getString("version") + lang.getString("CONSOLE_MESSAGE_DISABLED").split("%s")[1]);
	}

	public void registerCommands() {
		getCommand("randomteleport").setExecutor(new RandomTeleport(this));
	}

	public static File langf;
	public FileConfiguration lang;

	public void registerConfig() {

		try {
			switch (getConfig().getString("Language")) {
			case "es":
				langf = new File(getDataFolder(), "language_es.yml");
				if (!langf.exists()) {
					langf.getParentFile().mkdirs();
					copy(getResource("language_es.yml"), langf);
				}
				lang = new YamlConfiguration();

				break;
			default:
				langf = new File(getDataFolder(), "language_en.yml");
				if (!langf.exists()) {
					langf.getParentFile().mkdirs();
					copy(getResource("language_en.yml"), langf);
				}
				lang = new YamlConfiguration();
				break;

			}
		} catch (Exception e) {
			e.printStackTrace();
			langf = new File(getDataFolder(), "language_es.yml");
			if (!langf.exists()) {
				langf.getParentFile().mkdirs();
				copy(getResource("language_es.yml"), langf);
			}
			lang = new YamlConfiguration();
		}

		try {
			lang.load(langf);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void copy(InputStream in, File file) {

		try {

			OutputStream out = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {

				out.write(buf, 0, len);

			}
			out.close();
			in.close();

		} catch (Exception e) {

			e.printStackTrace();

		}

	}

}
